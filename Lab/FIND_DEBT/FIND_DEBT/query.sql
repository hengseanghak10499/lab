﻿USE [AUTO_UAT_OoneBillDb]

DROP TABLE IF EXISTS Z_checklist11214
DROP TABLE IF EXISTS Z_checklist11215
DROP TABLE IF EXISTS Z_customerusd
DROP TABLE IF EXISTS Z_customerkhr
DROP TABLE IF EXISTS Z_depositekhr
DROP TABLE IF EXISTS Z_createcashier
DROP TABLE IF EXISTS Z_Mics_HaveIDKHR
DROP TABLE IF EXISTS Z_Mics_NoID
DROP TABLE IF EXISTS Z_Report
DROP TABLE IF EXISTS Z_report

DECLARE @BranchId INT = CONVERT(INT,$(branchId));
--Checklist 11214
select top 2
[IN] = i.Code
,con.Code as ID
,con.Name
,con.Debt
,con.CurrencyId
,i.Status
into Z_checklist11214
from Installation i
INNER JOIN Consumer con  ON i.ConsumerId = con.Id
where BranchId = @BranchId  AND con.CurrencyId = 1 AND i.Status = 2
--Checklist 11215
select top 2
[IN] = i.Code
,con.Code as ID
,con.Name
,con.Debt
,con.CurrencyId
,i.Status
into Z_checklist11215
from Installation i
INNER JOIN Consumer con  ON i.ConsumerId = con.Id
where BranchId = @BranchId  AND con.CurrencyId = 2 AND i.Status = 2 AND con.Debt > 1

select top 3
con.Code as CustomerID
,con.Name
,Payment = CONVERT(float,(ROUND((con.Debt),0)))
,con.Debt
,Validate = FORMAT(CONVERT(float,((con.Debt) - ROUND((con.Debt),0))),'0.00')
,Shortcut = N'Ctrl+' + cast(ROW_NUMBER() OVER(ORDER BY con.Code ASC) as NVARCHAR)
,counter1 = N'បញ្ជរបង់ប្រាក់ ១០៣'
,[Index1] = N'4'
,codecashier = 3
into Z_customerusd
from Installation i
INNER JOIN Consumer con  ON i.ConsumerId = con.Id
where BranchId = @BranchId AND con.CurrencyId = 2 AND i.Status = 2 AND con.Debt > 1

select top 3 
(con.Code) as CustomerID
,con.Name 
,Payment = CONVERT(float,(ROUND((con.Debt/100),0)))*100
,con.Debt
,Validate = (con.Debt) - (CONVERT(float,(ROUND((con.Debt/100),0)))*100)
,Shortcut = N'Ctrl+' + cast(ROW_NUMBER() OVER(ORDER BY con.Code ASC) as NVARCHAR)
,counter1 = N'បញ្ជរបង់ប្រាក់ ១០១'
,[Index1] = N'2'
,codecashier = 1
into Z_customerkhr
from ConsumerDebt cb
INNER JOIN Consumer con ON con.Id = cb.ConsumerId
WHERE cb.DebtTypeId <> 2 AND cb.DebtTypeId <> 3
AND con.Debt > 1000 AND con.CurrencyId = 1
AND BranchId = @BranchId AND Archive = 0
group by con.Code,con.Name,con.Debt

SELECT top 3
con.Code as CustomerID
,con.Name
,CONVERT(float,(ROUND((cb.Balance/100),0)))*100 as Payment
,cb.Balance as Bond
,Validate = (cb.Balance)- (CONVERT(float,(ROUND((cb.Balance/100),0)))*100)
,Shortcutcash = N'Ctrl+1'
,counter1 = N'បញ្ជរបង់ប្រាក់ ១០២'
,[Index1] = N'3'
,codecashier = 2
into Z_depositekhr
FROM ConsumerDebt cb
INNER JOIN Installation ins ON ins.Id = cb.InstallationId
INNER JOIN Consumer con ON cb.ConsumerId = con.Id
WHERE DebtTypeId = 2 AND ins.[Status] = 2 AND Outstanding > 100 AND ins.BranchId = @BranchId
and con.CurrencyId = 1 AND Archive = 0

--Add Mics
select top 3
con.Code as CUSTOMERID
,PAYMENT = 15000
,SHORTCUT = N'Ctrl+' + cast(ROW_NUMBER() OVER(ORDER BY con.Code ASC) as NVARCHAR)
,NOTE = N'សំរាប់់តេស្តបន្ថែមបង្កាន់ដៃបង់ប្រាក់ផ្សេងៗមាន ID'
,DETAIL_MICS = N'RECR'
,CURRNECY = N'KHR'
into Z_Mics_HaveIDKHR
from Installation i
INNER JOIN Consumer con  ON i.ConsumerId = con.Id
where BranchId = 100  AND con.CurrencyId = 1 AND i.Status = 2

--Create Cashier
create table Z_createcashier
(
CODE_CASH int IDENTITY(1,1) PRIMARY KEY,
CODE_CASHIER varchar(255),
NAME_CASHIER nvarchar(255),
COUNTER_TYPE varchar(255),
CURRENCY varchar(255),
SELECT_ROW nvarchar(255)
)
INSERT INTO Z_createcashier (CODE_CASHIER, NAME_CASHIER, COUNTER_TYPE, CURRENCY, SELECT_ROW)
VALUES ('CASH1011', N'បញ្ជរបង់ប្រាក់ ១០១','Normal Counter','{Down}',N'ឈ្មោះ Row 0');
INSERT INTO Z_createcashier (CODE_CASHIER, NAME_CASHIER, COUNTER_TYPE, CURRENCY, SELECT_ROW)
values ('CASH1021-BNR', N'បញ្ជរបង់ប្រាក់ ១០២','Bond Counter','{Down}',N'ឈ្មោះ Row 1');
INSERT INTO Z_createcashier (CODE_CASHIER, NAME_CASHIER, COUNTER_TYPE, CURRENCY, SELECT_ROW)
VALUES ('CASH1031', N'បញ្ជរបង់ប្រាក់ ១០៣','Normal Counter','{Down}{Down}',N'ឈ្មោះ Row 0');

create table Z_Mics_NoID
(
PAYMENT varchar(255),
SHORTCUTCASE nvarchar(255),
NOTE nvarchar(255),
[INDEX] varchar(255),
DETAIL_MICS nvarchar(255),
CURRENCY varchar(255)
)

INSERT INTO Z_Mics_NoID(PAYMENT, SHORTCUTCASE, NOTE,[INDEX],DETAIL_MICS,CURRENCY)
VALUES ('2,010.00','Ctrl+1',N'តេស្តអូតូម៉ាតិច','4','DPU','USD');
INSERT INTO Z_Mics_NoID(PAYMENT, SHORTCUTCASE, NOTE,[INDEX],DETAIL_MICS,CURRENCY)
VALUES ('12.55','Ctrl+2',N'តេស្តអូតូម៉ាតិច','4','MIS-RNPU','USD');
INSERT INTO Z_Mics_NoID(PAYMENT, SHORTCUTCASE, NOTE,[INDEX],DETAIL_MICS,CURRENCY)
VALUES ('25.45','Ctrl+3',N'តេស្តអូតូម៉ាតិច','4','MIS','USD');
INSERT INTO Z_Mics_NoID(PAYMENT, SHORTCUTCASE, NOTE,[INDEX],DETAIL_MICS,CURRENCY)
VALUES ('10000','Ctrl+1',N'សំរាប់តេស្តអូតម៉ាតិច','2','RFR','KHR');
INSERT INTO Z_Mics_NoID(PAYMENT, SHORTCUTCASE, NOTE,[INDEX],DETAIL_MICS,CURRENCY)
VALUES ('15000','Ctrl+2',N'សំរាប់តេស្តអូតម៉ាតិច','2','RMS-METR','KHR');
INSERT INTO Z_Mics_NoID(PAYMENT, SHORTCUTCASE, NOTE,[INDEX],DETAIL_MICS,CURRENCY)
VALUES ('20000','Ctrl+3',N'សំរាប់តេស្តអូតម៉ាតិច','2','RMS-RNPR','KHR');

SELECT x.* 
  INTO Z_report
  FROM (select CODE_CASH,CODE_CASHIER,sum(ck.Payment) as Validate from Z_createcashier cc
join Z_customerkhr ck on cc.CODE_CASH = ck.codecashier
group by CODE_CASH,CODE_CASHIER
union all
select CODE_CASH,CODE_CASHIER,sum(cu.Payment) as Validate from Z_createcashier cc
join Z_customerusd cu on cc.CODE_CASH = cu.codecashier
group by CODE_CASH,CODE_CASHIER
union all
select CODE_CASH,CODE_CASHIER,sum(dk.Payment) as Validate from Z_createcashier cc
join Z_depositekhr dk on cc.CODE_CASH = dk.codecashier
group by CODE_CASH,CODE_CASHIER
) x

select
CODE_CASH,
CODE_CASHIER,
FORMAT(Validate,'#,#.00') as AMOUNT
into Z_Report
from Z_report 
order by CODE_CASH

