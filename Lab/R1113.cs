﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace Lab
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The R1113 recording.
    /// </summary>
    [TestModule("b2744383-e6e5-43e6-abf7-04b462f12492", ModuleType.Recording, 1)]
    public partial class R1113 : ITestModule
    {
        /// <summary>
        /// Holds an instance of the LabRepository repository.
        /// </summary>
        public static LabRepository repo = LabRepository.Instance;

        static R1113 instance = new R1113();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public R1113()
        {
            code_cashier = "cash_T2";
            N100 = "0";
            N50 = "0";
            N20 = "0";
            N10 = "0";
            N5 = "0";
            N2 = "0";
            N1 = "0";
            expect = "0 KHR";
            getAmount = "";
            returngetAmount = "";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static R1113 Instance
        {
            get { return instance; }
        }

#region Variables

        string _code_cashier;

        /// <summary>
        /// Gets or sets the value of variable code_cashier.
        /// </summary>
        [TestVariable("ddff0a3b-1bc9-4e00-be36-1a1b372ff436")]
        public string code_cashier
        {
            get { return _code_cashier; }
            set { _code_cashier = value; }
        }

        string _N100;

        /// <summary>
        /// Gets or sets the value of variable N100.
        /// </summary>
        [TestVariable("0fef0b66-428e-45c7-a437-c64cc871de41")]
        public string N100
        {
            get { return _N100; }
            set { _N100 = value; }
        }

        string _N50;

        /// <summary>
        /// Gets or sets the value of variable N50.
        /// </summary>
        [TestVariable("dc342535-b28b-4796-8d84-5203d8fc18f9")]
        public string N50
        {
            get { return _N50; }
            set { _N50 = value; }
        }

        string _N20;

        /// <summary>
        /// Gets or sets the value of variable N20.
        /// </summary>
        [TestVariable("03fd12e9-42a4-437f-a70a-1c467c306d53")]
        public string N20
        {
            get { return _N20; }
            set { _N20 = value; }
        }

        string _N10;

        /// <summary>
        /// Gets or sets the value of variable N10.
        /// </summary>
        [TestVariable("745bfb0a-a00a-400a-b4e4-3ae874c613e4")]
        public string N10
        {
            get { return _N10; }
            set { _N10 = value; }
        }

        string _N5;

        /// <summary>
        /// Gets or sets the value of variable N5.
        /// </summary>
        [TestVariable("a30d4c69-60bc-47a8-a322-62753c2b0c7d")]
        public string N5
        {
            get { return _N5; }
            set { _N5 = value; }
        }

        string _N2;

        /// <summary>
        /// Gets or sets the value of variable N2.
        /// </summary>
        [TestVariable("aa7ad845-35d0-444b-98b5-d984fe937d9f")]
        public string N2
        {
            get { return _N2; }
            set { _N2 = value; }
        }

        string _N1;

        /// <summary>
        /// Gets or sets the value of variable N1.
        /// </summary>
        [TestVariable("eee8e83b-2455-42a5-97ec-aa8288adc242")]
        public string N1
        {
            get { return _N1; }
            set { _N1 = value; }
        }

        string _expect;

        /// <summary>
        /// Gets or sets the value of variable expect.
        /// </summary>
        [TestVariable("55fc7f04-d92c-4091-a629-fe5113f4fc28")]
        public string expect
        {
            get { return _expect; }
            set { _expect = value; }
        }

        string _getAmount;

        /// <summary>
        /// Gets or sets the value of variable getAmount.
        /// </summary>
        [TestVariable("191166d0-ba38-4ab5-95d7-a3257907e8d2")]
        public string getAmount
        {
            get { return _getAmount; }
            set { _getAmount = value; }
        }

        string _returngetAmount;

        /// <summary>
        /// Gets or sets the value of variable returngetAmount.
        /// </summary>
        [TestVariable("1e13820a-ce94-495f-a392-639174872cc4")]
        public string returngetAmount
        {
            get { return _returngetAmount; }
            set { _returngetAmount = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.StormPaymentInterfacesCounterSessionP' at 70;14.", repo.MainForm.MainPanel.StormPaymentInterfacesCounterSessionPInfo, new RecordItemIndex(0));
            repo.MainForm.MainPanel.StormPaymentInterfacesCounterSessionP.Click("70;14");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Text to '$code_cashier' on item 'MainForm.MainPanel.TxtSearch82'.", repo.MainForm.MainPanel.TxtSearch82Info, new RecordItemIndex(1));
            repo.MainForm.MainPanel.TxtSearch82.Element.SetAttributeValue("Text", code_cashier);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.Button72' at 11;9.", repo.MainForm.MainPanel.Button72Info, new RecordItemIndex(2));
            repo.MainForm.MainPanel.Button72.Click("11;9");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 300ms.", new RecordItemIndex(3));
            Delay.Duration(300, false);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.BtnCloseCounter' at 39;18.", repo.MainForm.MainPanel.BtnCloseCounterInfo, new RecordItemIndex(4));
            repo.MainForm.MainPanel.BtnCloseCounter.Click("39;18");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 300ms.", new RecordItemIndex(5));
            Delay.Duration(300, false);
            
            Report.Log(ReportLevel.Info, "Get Value", "Getting attribute 'Text' from item 'Copy_of_CounterSessionCashNoteDailog.LblTotalAmount' and assigning its value to variable 'getAmount'.", repo.Copy_of_CounterSessionCashNoteDailog.LblTotalAmountInfo, new RecordItemIndex(6));
            getAmount = repo.Copy_of_CounterSessionCashNoteDailog.LblTotalAmount.Element.GetAttributeValueText("Text");
            Delay.Milliseconds(100);
            
            returngetAmount = cutstring();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "User", returngetAmount, new RecordItemIndex(8));
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 300ms.", new RecordItemIndex(9));
            Delay.Duration(300, false);
            
            countCurrency();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow1' at 82;11.", repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow1Info, new RecordItemIndex(11));
            repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow1.Click("82;11");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$N100' with focus on 'CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow1'.", repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow1Info, new RecordItemIndex(12));
            repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow1.PressKeys(N100);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow2' at 96;12.", repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow2Info, new RecordItemIndex(13));
            repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow2.Click("96;12");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$N50' with focus on 'CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow2'.", repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow2Info, new RecordItemIndex(14));
            repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow2.PressKeys(N50);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow3' at 95;13.", repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow3Info, new RecordItemIndex(15));
            repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow3.Click("95;13");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$N20' with focus on 'CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow3'.", repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow3Info, new RecordItemIndex(16));
            repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow3.PressKeys(N20);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow4' at 84;23.", repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow4Info, new RecordItemIndex(17));
            repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow4.Click("84;23");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$N10' with focus on 'CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow4'.", repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow4Info, new RecordItemIndex(18));
            repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow4.PressKeys(N10);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow5' at 87;19.", repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow5Info, new RecordItemIndex(19));
            repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow5.Click("87;19");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$N5' with focus on 'CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow5'.", repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow5Info, new RecordItemIndex(20));
            repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow5.PressKeys(N5);
            Delay.Milliseconds(0);
            
            //Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow6' at 90;13.", repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow6Info, new RecordItemIndex(21));
            //repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow6.Click("90;13");
            //Delay.Milliseconds(0);
            
            //Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$N2' with focus on 'CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow6'.", repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow6Info, new RecordItemIndex(22));
            //repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow6.PressKeys(N2);
            //Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow7' at 92;14.", repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow7Info, new RecordItemIndex(23));
            repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow7.Click("92;14");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$N1' with focus on 'CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow7'.", repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow7Info, new RecordItemIndex(24));
            repo.CounterSessionCashNoteDailog.TableDgv.ចននសនលកRow7.PressKeys(N1);
            Delay.Milliseconds(0);
            
            try {
                //Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (Text=$expect) on item 'Copy_of_CounterSessionCashNoteDailog.LblTotalAmount'.", repo.Copy_of_CounterSessionCashNoteDailog.LblTotalAmountInfo, new RecordItemIndex(25));
                //Validate.AttributeEqual(repo.Copy_of_CounterSessionCashNoteDailog.LblTotalAmountInfo, "Text", expect, null, false);
                //Delay.Milliseconds(100);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(25)); }
            
            try {
                //Report.Log(ReportLevel.Info, "Validation", "(Optional Action)\r\nValidating AttributeEqual (Text=$expect) on item 'CounterSessionCashNoteDailog.LblActualAmount'.", repo.CounterSessionCashNoteDailog.LblActualAmountInfo, new RecordItemIndex(26));
                //Validate.AttributeEqual(repo.CounterSessionCashNoteDailog.LblActualAmountInfo, "Text", expect, null, false);
                //Delay.Milliseconds(100);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(26)); }
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CounterSessionCashNoteDailog.BtnOK' at 56;15.", repo.CounterSessionCashNoteDailog.BtnOKInfo, new RecordItemIndex(27));
            repo.CounterSessionCashNoteDailog.BtnOK.Click("56;15");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 1s.", new RecordItemIndex(28));
            Delay.Duration(1000, false);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'CloseCounterSessionDialog.BtnSave' at 56;10.", repo.CloseCounterSessionDialog.BtnSaveInfo, new RecordItemIndex(29));
            repo.CloseCounterSessionDialog.BtnSave.Click("56;10");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Delay", "Waiting for 1s.", new RecordItemIndex(30));
            Delay.Duration(1000, false);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'ExMessage.BtnYes' at 53;18.", repo.ExMessage.BtnYesInfo, new RecordItemIndex(31));
            repo.ExMessage.BtnYes.Click("53;18");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'MainForm.MainPanel.Button72' at 15;12.", repo.MainForm.MainPanel.Button72Info, new RecordItemIndex(32));
            repo.MainForm.MainPanel.Button72.Click("15;12");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
