﻿/*
 * Created by Ranorex
 * User: USER
 * Date: 12/21/2020
 * Time: 2:57 PM
 * 
 * To change this template use Tools > Options > Coding > Edit standard headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;

namespace Lab
{
    /// <summary>
    /// Description of R1312.
    /// </summary>
    [TestModule("F407B27F-4BA9-4A72-BE20-7E1F00A7163E", ModuleType.UserCode, 1)]
    public class R1312 : ITestModule
    {
        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public R1312()
        {
            // Do not delete - a parameterless constructor is required!
        }

        /// <summary>
        /// Performs the playback of actions in this module.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.0;
        }
    }
}
